var express = require("express");
var app = express();

var morgan = require('morgan');
var bodyParser = require('body-parser');
var serveStatic = require('serve-static');

var listaPalabras = [];

app.use(serveStatic('public', { 'index': ['default.html', 'default.htm'] }));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
 
// parse application/json
app.use(bodyParser.json());
app.use(morgan('combined'));


app.post("/removeduplicatewords", function(req, res) {
    
    listaPalabras = req.body['palabras'];
    var listaPalabrasSplit = listaPalabras.split(",");

    var count = 1;
    listaPalabrasSplit.forEach(palabra => {
        
        for (let i = count; i < listaPalabrasSplit.length; i++){
            if(palabra === listaPalabrasSplit[i]){
                listaPalabrasSplit.splice(i, 1);
                i--;
            }
        }
        count++;
    });
    

    res.send(listaPalabrasSplit)
});

var primerConsult = true;
var consultPost = "";
app.get("/botorder/:order1", function(req, res) {
  
    if(primerConsult){
        primerConsult = false;
        res.send("NONE")
    } else {
        res.send(consultPost)
    }

});

app.post("/botorder/:order1", function(req, res) {
  
    consultPost = req.body['botorder'];
    res.send(200);
});

app.use(function(req,res){
    res.status(404);
    res.send("default 404 webpage");
});



app.listen(8000);